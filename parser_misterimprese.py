import requests
from bs4 import BeautifulSoup
import re
from multiprocessing.dummy import Pool as ThreadPool
from PIL import Image
import tesserocr
import random
import csv

BASE_URL = "http://www.misterimprese.it"
tread_count = 50

proxy_list = []
reader = csv.reader(open("proxy_list.csv", "rt"))
for row in reader:
    proxy_list.append(row)


def get_html_page(url):
    while True:
        ind = random.randint(0,len(proxy_list)-1)
        proxy = {proxy_list[ind][0]: proxy_list[ind][1]}
        try:    
            r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'}, proxies=proxy, timeout=1)
        except requests.exceptions.RequestException:
            html = None
        else:
            if r.ok:
                html = r.text
                break
    return html


def get_city_list(url):
    print('get_city_list')
    #r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    #html = r.text
    html = get_html_page(url)
    soup = BeautifulSoup(html)
    table = soup.find_all('div', class_="citylist col-xs-12 col-md-6 col-lg-4 ")
    cities = []
    for row in table:
        city = row.find('a', href=True)
        cities.append({"city": city.text,
                       "href": city['href']})
    return cities

def get_category_list(url):
    print('get_category_list')
    #r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    #html = r.text
    html = get_html_page(url)
    soup = BeautifulSoup(html)
    table = soup.find_all('div', class_="col-sm-6 col-xs-12 compRelated citylist")
    categories = []
    for row in table:
        category = row.find('a', href=True)
        categories.append({"category": category.text,
                       "href": category['href']})
    return categories

def get_firm_list(url):
    print('get_firm_list')
    firm_urls = []
    while True:
        #r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
        #html = r.text
        html = get_html_page(url)
        soup = BeautifulSoup(html)
        table = soup.find_all('div', class_="company-data")
        
        for row in table:
            firm_urls.append(row.find('a', href=True)['href'])
        table = soup.find('ul', class_="pagination ")
        
        if table is None:
            break
        a = table.find_all('li')[-1].find('a', href=True)
        if a is None:
            break
        if a.text == '»':
            next_url = a['href']
        else:
            break
        url = 'http://www.misterimprese.it' + next_url
    return firm_urls

def get_firm_info(url):
    #r = requests.get(url, headers={'User-Agent': 'Mozilla/5.0'})
    #html = r.text
    html = get_html_page(url)
    result = re.findall(r'#Iwo3ahTu.*"/([^=]*)=', html)
    url_img = "http://www.misterimprese.it/img/"+result[0]

    p = requests.get(url_img, headers={'User-Agent': 'Mozilla/5.0'})
    out = open("email.png", "wb")
    out.write(p.content)
    out.close()

    image1 = Image.open('email.png')

    nx, ny = image1.size
    im2 = image1.resize((int(nx*5), int(ny*5)), Image.BICUBIC)

    email = tesserocr.image_to_text(im2)

    soup = BeautifulSoup(html)
    table = soup.find('div', id="companyDetails")
    name = table.find('h2').get_text()
    return {"name": name,
            "email": email,
            }


# get page
print('scraping', BASE_URL)
r = requests.get(BASE_URL, headers={'User-Agent': 'Mozilla/5.0'})
html = r.text
print('parse region')
# parse region
soup = BeautifulSoup(html)
table = soup.find_all('div', class_="cont")
table = table[1].find_all('a', href=True)
region_url = []
for row in table:
    region_url.append(BASE_URL+row['href'])
print('find', len(table), 'region')
# parse cities
print('parse cities')
pool = ThreadPool(tread_count)
cities = pool.map(get_city_list, region_url)
pool.close()
pool.join()
city_url = []
for row in cities:
    for elem in row:
        city_url.append(BASE_URL + elem['href'])
print('find', len(city_url), 'cities')
# parse categories
print('parse categories')
pool = ThreadPool(tread_count)
categories = pool.map(get_category_list, city_url)
pool.close()
pool.join()
category_url = []
for row in categories:
    for elem in row:
        category_url.append(BASE_URL + elem['href'])
print('find', len(category_url), 'categories')
# parse firms href
print('parse firms urls')
pool = ThreadPool(tread_count)
firms_url = pool.map(get_firm_list, category_url)
pool.close()
pool.join()
print('find', len(firms_url), 'firms')
# parse firms href
print('parse firms information')
pool = ThreadPool(tread_count)
firm_info = pool.map(get_firm_info, firms_url)
pool.close()
pool.join()

print(firm_info)


