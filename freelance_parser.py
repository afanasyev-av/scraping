#parse freelance.com python 2.7
import urllib2
from bs4 import BeautifulSoup

BASE_URL = 'http://www.freelance.com'


def get_html(url):
    response = urllib2.urlopen(url)
    return response.read()


def get_page_count(html):
    soup = BeautifulSoup(html)
    pagination = soup.find('ul', class_='pagination')
    return int(pagination.find_all('a')[-1].text)


def parse(html):
    soup = BeautifulSoup(html)
    table = soup.find('div', class_='jobsearch-result')
    projects = []
    for row in table.find_all('div', class_='jobsearch-result-list'):
        date = row.find('div',class_='col-xs-6 col-md-2 col-lg-2 lefttop').text
        date = date.replace('\n','')
        date = date.split('\r')
        date = date[2].strip()
        date_start = row.find('div', class_='col-xs-6 col-md-2 col-lg-2 righttop')
        title = row.find('div', class_='col-xs-12 col-md-4 col-lg-4 center')
        skill_str = title.div.text.replace('\r\n','').split('|')
        skills = []
        for skill in skill_str:
            skills.append(skill.strip())
        projects.append({
            'title': title.a.text,
            'href': BASE_URL + title.a['href'],
            'data_pub':date,
            'skill': skills
        })
    return projects


def main():
    html = get_html(BASE_URL)
    soup = BeautifulSoup(html)
    href_table = soup.find_all('a', href=True)
    url = None
    for href in href_table:
        href_str = href['href']
        if href_str.find('search') != -1:
            url = href_str
            break
    page_count = get_page_count(get_html(url))
    projects = []
    for page in range(1,page_count+1):
        print 'parsing %d%%' %(page*100/page_count)
        html = get_html(url+ '?page=%d' % page)
        projects.extend(parse(html))
    for project in projects:    
        print project

    
if __name__ == '__main__':
    main();
