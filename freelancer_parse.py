#scraping freelancer.com
#python 3.4
from robobrowser import RoboBrowser
import re
from datetime import datetime, timedelta
import time
import openpyxl


base_url = "https://www.freelancer.com"
wb=openpyxl.Workbook()
ws = wb.create_sheet(0)
print('scraping freelancer.com')
user = input('user name: ')
password = input('password: ')

browser = RoboBrowser(history=True)
browser.open(base_url)

formSearch = browser.get_form(id='login-form')
formSearch['username'].value = user
formSearch['passwd'].value = password
browser.submit_form(formSearch)

base_url = "https://www.freelancer.com/jobs"
browser.open(base_url)
html = browser.parsed
pages = html.find('div',id='project_table_static_paginate')
page = pages.find_all('a',href=True)
num = re.search(r'[\d]+',page[-1]['href'])
endpage = int(num.group(0))

res_row = ['name','href','bids','start','end','price','skils']
ws.append(res_row)

for i in range(1, endpage):
    url = base_url + '/' + str(i)
    browser.open(url)
    html = browser.parsed
    
    table = html.find_all('tr',class_='ProjectTable-row project-details odd')
    table.extend(html.find_all('tr',class_='ProjectTable-row project-details even'))
    for row in table:
        td = row.find_all('td')
        a = td[0].find('a',href=True)
        name = a.text
        print('==================================')
        print(name)
        href = a['href']
        print(href)
        description = td[1].text
        print(description)
        bids = td[2].text
        print(bids)
        skils = td[3].text
        print(skils)
        start = td[4].text
        conv=time.strptime(start,"%b %d, %Y")
        start = time.strftime("%d/%m/%Y",conv)
        print(start)
        end = td[5].find('small').text
        dates = re.findall(r'[\d]+',end)
        now_time = datetime.now()
        end = now_time + timedelta(days=int(dates[0]), hours=int(dates[1]))
        end = end.strftime("%d/%m/%Y")
        print(end)
        price = td[6].text
        print(price)
        res_row = [name,href,bids,start,end,price,skils]
        ws.append(res_row) 
wb.save(filename='freelancer.xlsx')
